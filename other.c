#include "stdio.h"
#include "string.h"

/*get_next:
	fills array int *arr with the next combination of numbers
	returns 0 when no errors
	returns -1 when you reach past the last combination
*/
int get_next(int *arr, int size);

void print(int *res, int size);
void increment_i(int *arr, int i, int size, int *parking);

void print(int *res, int size){

	int i = 0;

	printf("[ ");
	while(i < size){
		printf("%d ", res[i]);
		i += 1;
	}

	printf("]\n");

}//print*/

void increment_i(int *arr, int i, int size, int *parking){

	parking[arr[i] - 1] = arr[i];
	arr[i] += 1;

	//check whether we find it in test
	int found = 0;
	while(!found){
		if( parking[arr[i] - 1] ){
			parking[arr[i] - 1] = 0;
			found = 1;
		}else{
			arr[i] += 1;
		}

	}

}//increment_i*/

int get_next(int *arr, int size){

	int parking[size];
	memset(parking, 0, size * sizeof(int));

	int prev = arr[size-1];
	parking[prev - 1] = prev;
	arr[size-1] = 0;
	int i = size - 2;
	while(i && arr[i] > prev){
		prev = arr[i];
		parking[prev - 1] = prev;
		arr[i] = 0;
		i -= 1;
	}

	if(arr[i] == size){
		if(!i){
			return -1;
		}
		i -= 1;
	}

	increment_i(arr, i, size, parking);

	i += 1;
	while(i < size){
		int j = 0;
		while(!parking[j]){
			j += 1;
		}
		arr[i] = j + 1;
		parking[j] = 0;
		i += 1;
	}

	return 0;

}//get_next*/

int main(){

	#define SIZE 5
	int arr[SIZE] = {1,2,3,4,5};

	printf("before get next\n");
	print(arr, SIZE);

	int i = 0;
	while(!get_next(arr, SIZE)){
		print(arr, SIZE);
		i += 1;
	}

	return 0;

}//main*/
