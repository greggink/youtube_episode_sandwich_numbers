#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"

#define N 4

#define MAX_SIZE 100
uint64_t all_solutions = 0;

#define increment_i(X,Y) X+=1;if(X == Y){X = 0;}

void list_combinations(int n);
void lc(int *used, int i, int size, int *result);
int find_first(int *used, int size);
int find_next(int *used, int size, int level);
void expand(int *result, int n);
void print(int *res, int size);

int main(){

	printf("computing for n = %d\n", N);
	list_combinations(N);
	printf("all solutions for %d is %lu\n", N, all_solutions);

	return 0;

}//main*/

void list_combinations(int n){

	int used[n];
	memset(used, 0, sizeof(int) * n);
	int result[n];
	memset(result, 0, sizeof(int) * n);

	lc(used, 0, n, result);

}//list_combinations*/

void lc(int *used, int i, int size, int *result){

	if(i < 0 || i == size)
		return;

	int level = i + 1;

	int k = find_first(used, size);
	int last = k;
	int go = size - i;
	while(go){
		result[i] = k;
		k -= 1;
		used[k] = level;

		lc(used, level, size, result);
		if(level == size){
			expand(result, size);
//			print(result, size);
		}
		go -= 1;
		last = k;
		k = find_next(used, size, level);
	}

	used[last] = 0;

}//lc*/

void expand(int *result, int n){

	int test[n*2];
	memset(test, 0, sizeof(int) * 2 * n);

	int i = 0;
	int k = 0;
	int twice = n*2;

	//fill in array
	while(i < n){
		k = result[i];

		int z = 0;
		while(test[z] && z < twice){
			z += 1;
		}
		if(z >= twice){
			printf("ooopsie\n");
			exit(1);
			return;
		}

		test[z] = k;
		z += k + 1;
		if(z >= twice){
			return;
		}
		test[z] = k;

		i += 1;
	}

	//check for zeroes
	i = 0;
	while(i < twice){
		if(!test[i]){
			return;
		}
		i += 1;
	}

	all_solutions += 1;
//	print(result, n);
	print(test, twice);

}//expand*/

int find_first(int *used, int size){

	int i = 0;

	while(used[i] && i < size){
		i += 1;
	}

	return i + 1;

}//find_first*/

int find_next(int *used, int size, int level){

	int i = 0;

	while(used[i] != level && i < size){
		i += 1;
	}
	if(i >= size || used[i] != level)
		return -1;

	used[i] = 0;
	increment_i(i, size);
	while(used[i]){
		increment_i(i, size);
	}

	return i + 1;

}//find_next*/

void print(int *res, int size){

	int i = 0;

	printf("[ ");
	while(i < size){
		printf("%d ", res[i]);
		i += 1;
	}

	printf("]\n");

}//print*/
